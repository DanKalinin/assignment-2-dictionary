NASM = nasm -f elf64 -o

.PHONY: clean
clean:
	rm -f main
	rm -f *.o

lib.o: lib.asm
	$(NASM) lib.o lib.asm

dict.o: dict.asm
	$(NASM) dict.o dict.asm

main.o: main.asm colon.inc words.inc
	$(NASM) main.o main.asm


main: main.o lib.o dict.o
	ld -o main main.o lib.o dict.o


