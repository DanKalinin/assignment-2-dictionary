global find_word
extern string_equals

;	rdi - указатель на нультерменированную строку
;	rsi - указатель на начало словаря
;	Возвращает адрес найенного значения или 0

find_word:
	xor rax, rax
	
	.loop:
		push rdi	
		push rsi

		add rsi, 8		;запишем в rsi адрес ключа
		call string_equals

		pop rsi
		pop rdi

		test rax, rax
		je .not_equals
		jne .equals
		
	.not_equals:
		mov r8, qword [rsi]
		test r8, r8		;проверяем есть ли следующий ключ
		je .return_false	;если нет, то выходим с false
		
		mov rsi, [rsi]		;если да, то продолжаем поиск
		jmp .loop

	.return_false:
		mov rax, 0
		ret

	.equals:
		add rsi, 8		;указатель на ключ
		mov rax, rsi
		ret

	
	
