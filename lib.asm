global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov  rax, 60             ; 'exit' syscall number
    xor  rdi, rdi
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .count:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .count
    .end:
	    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov  rdx, rax
    mov  rsi, rdi
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi      
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    mov rsi, rsp   
    syscall
    pop rdi 
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r12	;сохранение предыдущих значений
    push r13
	
    mov r12, 10 ;основание системы сичисления
    mov r13, rsp ;сохраняем вершину стека
	
    push 0
    mov rax, rdi ;пишем число в аккумулятор

    .loop: 
	    mov rdx, 0
        div r12				;записываем в rdx остаток от деления на 10
        add rdx, '0' 		;переводим символ в ASCII
        dec rsp 			;готовим место под новый символ строки
        mov byte[rsp], dl 	;записываем символ в стек
        cmp rax, 0 			;проверяем конец числа
        je .end
        jmp .loop

    .end:
        mov rdi, rsp 		;передаем в print_string указатель строки
    	push rax
        call print_string
	    pop rax
        mov rsp, r13 		;восстанавливаем изначальный rsp
	    pop r13 			;восстанавливаем регистры
	    pop r12
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    test rax, rax
    jns .positive

    .negative:
        mov  rdi, '-' 
        push rax
        call print_char	
        pop rax

        neg rax	
        mov rdi, rax

        jmp print_uint

    .positive:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push r12
    push r13
    .loop:
        mov r12b, byte[rdi]
        mov r13b, byte[rsi]
            
        cmp r12b, r13b
        jne .false

        inc rsi
        inc rdi

        cmp r13b, 0
        je .true
        jne .loop

    .true:
        mov rax, 1
        jmp .end

    .false:
        mov rax, 0
        jmp .end

    .end:
        pop r12
        pop r13
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rdx
    mov rdi, 0
    mov rdx, 1
    push 0
    mov rsi, rsp
    syscall
    pop rax
    pop rdx
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax		    ;rdi - адрес начала буфера, rsi - размер буфера
    push rcx 			    ;rcx - итератор символов строки
    mov rcx, 0

    .loop:
        push rcx
        push rdi
        push rsi
        call read_char	    ;читаем символ строки в rax
        pop rsi
        pop rdi
        pop rcx
        
        cmp rax, 0 		    ;проверка на конец строки
        je .end

        cmp rax, ` ` 	    ;проверки на пустые символы
        je .space
        cmp rax, `\t`
        je .space
        cmp rax, `\n`
        je .space
		
        mov [rdi + rcx], rax	;пишем в память новый символ в следующую ячейку
        inc rcx
            
        cmp rcx, rsi	    ;проверка на размер буфера r12 >= rsi -> error
        jge .error
            
        jmp .loop

    .space:
	    cmp rcx, 0
        je .loop
        jmp .end

    .error:
        pop rcx
        mov rax, 0
        mov rdx, 0
        ret

    .end:
	    mov rax, 0
        mov [rdi+rcx], rax
        mov rax, rdi 
        mov rdx, rcx
	    pop rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax				;rax = 0
    mov rcx, 0					;итератор rcx = 0
    mov rdx, 0					;длина числа rdx = 0
    mov rsi, 0					;символ строки rsi = 0

    push r10
    mov r10, 10					;система счисления r10 = 10
    
    .loop:
        mov sil, [rdi + rcx]	;читаем символ

        cmp sil, '0'			;проверка на число, символ должен лежать '0' < sil < '9'
        jl .end
        cmp sil, '9'
        jg .end

        sub sil, '0'			;из символа в число
        mul r10				    ;добавляем к аккумулятору новую цифру
        add rax, rsi

        inc rcx
        jmp .loop

    .end:
        mov rdx, rcx			;пишем длину в rdx
        pop r10
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax				;rdi - указатель на строку
    xor rdx, rdx

    mov rcx, 0					;rcx - итератор

    cmp byte[rdi + rcx], '-'	;проверка на отрицательность
    jne .positive

    .negative:
        inc rcx
        add rdi, rcx 			;формируем новый указатель строки без минуса

        push rcx
        call parse_uint 		;парсим беззнаковую строку в rax и его длину в rdx
        pop rcx

        neg rax					;приводим число к отрицательному виду 
	    inc rdx					;увеличиваем длину числа с учетом минуса
        ret

    .positive:
        jmp parse_uint 			;парсим беззнаковую строку в rax и его длину в rdx

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	;rdi - указатель на строку, rsi - указатель на буфер, rdx - длина буфера
    push rdi
    push rsi
    push rdx

    call string_length		;в rax длина строки

    pop rdx
    pop rsi
    pop rdi

    cmp rax, rdx			;проверка на умещаемость строки в буфер
    jg .error

    mov rcx, 0				;rcx - итератор
    push r12

    .loop:
        mov r12b, [rdi+rcx]	;читаем символ из строки
        mov [rsi+rcx], r12b	;пишем символ в буфер

        cmp rcx, rax		;проверяем не конец ли строки
        je .end
        inc rcx

        jmp .loop

    .error:
        xor rax, rax		;пишем ошибку в rax
        ret

    .end:
        pop r12
        ret
