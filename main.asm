global _start
	
%include "colon.inc"
%include "words.inc"

extern read_word
extern find_word
extern string_length
extern print_string
extern print_newline
extern exit	

section .data
buffer:
	times 256 db 0

buff_err:
	db "Buffer error", 0

key_not_found_err:
	db "Key not found error", 0

section .text


_start: 
	xor rax, rax
	mov rsi, 255		;размер буфера
	
	mov rdi, buffer		;сслыка на начало буфера
	call read_word		;пишем слово в rax, его длину в rdx
	
	test rax, rax
	je .print_buffer_error
	jmp .check_word
	
.print_buffer_error:
	mov rdi, buff_err
	jmp .print_error

.print_key_not_found_error:
	mov rdi, key_not_found_err
	jmp .print_error

.print_error:
	call string_length
    mov  rdx, rax		;длина строки
    mov  rsi, rdi		;указатель на строку
    mov  rax, 1			;'write'
    mov  rdi, 2			;пишем в stderr
    syscall
	call print_newline
	call exit

.check_word:
	mov rdi, rax			;пишем начало прочитанного слова в rdi
	mov rsi, first_word		;пишем указатель на первый элемент словаря

	call find_word			;возвращает указатель на строку
	test rax, rax
	je .print_key_not_found_error	;если 0, то сообщить ошибку 

	add rax, rdx				; указатель на строку-значение по ключу
	add rax, 1

	mov rdi, rax
	call print_string
	call print_newline	
	call exit
